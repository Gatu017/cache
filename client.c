#include "networking.h"

void cli_loop(int sock, char *s){
    struct sockaddr_storage their_addr;
    socklen_t sin_size = sizeof their_addr;

    char *prefix;
    asprintf(&prefix, "%s:%s>>", s, PORT);
    char line[1024];
    while (1){
        printf("%s ", prefix);
        if (!fgets(line, sizeof line, stdin)){
            printf("\n");
            break;            
        }
        send_msg(sock, line);
    }
}

int main()
{
    char *s = (char *) calloc(INET6_ADDRSTRLEN, sizeof(char));
    //char s[INET6_ADDRSTRLEN];
    int sock = create_client_socket("hp", &s);
    if (sock == -1){
        perror("Socket failure");
        exit(1);
    }
    cli_loop(sock, s);
    close(sock);
}
