#include "networking.h"
#include <pthread.h>

#define THREAD_COUNT 10

pthread_t threads[THREAD_COUNT];
pthread_mutex_t mlock = PTHREAD_MUTEX_INITIALIZER;
int server_socket;

void handle_client(int socket)
{
    //TODO
    while (1){
        char *test_msg = read_msg(socket);
        printf("%s\n", test_msg);
    }
}

void *server_loop(void *targ)
{
    pthread_detach(pthread_self());
    struct sockaddr_storage their_addr;
    socklen_t sin_size = sizeof their_addr;
    char s[INET6_ADDRSTRLEN];

    while (1){
        pthread_mutex_lock(&mlock);

        int client_socket = accept(server_socket, (struct sockaddr *)&their_addr, &sin_size);
        if (client_socket == -1){
            // TODO error
            pthread_mutex_unlock(&mlock);
            continue;
        }
        
        inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s);
        printf("Connection from %s\n", s);

        pthread_mutex_unlock(&mlock);
        handle_client(client_socket);
    }
}

void create_threads(int index)
{
    pthread_create(&threads[index], NULL, &server_loop, NULL);
}

int main(){
    server_socket = create_server_socket();
    signal(SIGPIPE, SIG_DFL);
    for (int i = 0; i < THREAD_COUNT; i++){
        create_threads(i);
    }
    for (;;)
        pause();
}
