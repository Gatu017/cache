#include "networking.h"


// Loop to send all data through socket sock
int sendall(int sock, void *buf, int len)
{
    int total = 0;
    int bytesleft = len;
    int n;
    while (total < len){
        n = send(sock, buf + total, bytesleft, 0);
        if (n == -1)
            break;
        total += n;
        bytesleft -= n;
    }
    return n == -1 ? -1 : 0;
}

// Loop to recv all data through socket sock
int recvall(int sock, void *buf, int len)
{
    int total = 0;
    int bytesleft = len;
    int n;
    while (total < len){
        n = recv(sock, buf + total, bytesleft, 0);
        if (n == -1)
            break;
        total += n;
        bytesleft -= n;
    }
    return n == -1 ? -1 : 0;
}

// get sockaddr, IPv4 or IPv6
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET){
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

int create_server_socket()
{
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int yes = 1;
    int rv;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0){
        // TODO error 
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return -1;
    }

    for (p = servinfo; p != NULL; p = p->ai_next){
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
            // TODO error
            continue;
        }

        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
            // TODO error 
            close(sockfd);
            continue;
        }

        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1){
            close(sockfd);
            // TODO error
            continue;
        }
        break;
    }

    freeaddrinfo(servinfo);

    if (p == NULL){
        // TODO error
        return -1;
    }

    if (listen(sockfd, BACKLOG) == -1){
        // TODO error
        return -1;
    }
    return sockfd;
}

int create_client_socket(char *address, char **addr_buf)
{
    int sockfd;
    struct addrinfo hints, *servinfo, *p;
    int rv;
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;


    if ((rv = getaddrinfo(address, PORT, &hints, &servinfo)) != 0){
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return -1;
    }

    for (p = servinfo; p != NULL; p = p->ai_next){
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1){
            continue;
        }

        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1){
            close(sockfd);
            continue;
        }
        break;
    }

    if (p == NULL){
        //TODO
        return -1;
    }

    char s[INET6_ADDRSTRLEN];
    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr), s, sizeof s);
    *addr_buf = s;

    freeaddrinfo(servinfo);
    return sockfd;
}

char *read_msg(int sock)
{
    uint16_t length;
    int len;
    if (recvall(sock, &length, sizeof(length)) == -1){
        //TODO error
        return NULL;
    }
    len = ntohs(length);
    char *buffer = (char *) malloc(len);
    if (recvall(sock, buffer, len) == -1){
        // TODO error
        return NULL;
    }
    return buffer;
}

int send_msg(int sock, char *msg)
{
    int len = strlen(msg);
    uint16_t length = htons(len);
    if (sendall(sock, &length, sizeof(length)) == -1){
        //TODO error
        return -1;
    }
    if (sendall(sock, msg, len) == -1){
        //TODO error
        return -1;
    }
    return 0;
}
