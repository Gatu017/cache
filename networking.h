#ifndef CACHE_NETWORKING_H
#define CACHE_NETWORKING_H
#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>

#define PORT "3490"
#define BACKLOG 10


int sendall(int sock, void *buf, int len);
int recvall(int sock, void *buf, int len);
int create_server_socket();
int create_client_socket(char *address, char **add_buf);
void *get_in_addr(struct sockaddr *sa);
char *read_msg(int sock);
int send_msg(int sock, char *msg);

#endif
