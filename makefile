CC=gcc
CFLAGS='I. -Wall'
DEPS = networking.h
all: cache-server cache-cli

cache-server: server.c  networking.c
	$(CC) -o cache-server server.c networking.c -lpthread
cache-cli: client.c networking.c
	$(CC) -o cache-cli client.c networking.c
